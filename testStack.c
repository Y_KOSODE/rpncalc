// testStack.c by *****

#include <stdio.h>
#include "stack.h"
#include "testCommon.h"
#define N 10000

void testinitStack();
void testTopValue();
void testPush();
void testPull();
void testPrintStack();

void testinitStack() {
    stack myStack;
    testStart("initStack");
    myStack.top = 0; // 適当な値にセット
    initStack(&myStack, N);
    assert_equals_int(myStack.top, N); // sp が最終値より後ろになっているか
    testEnd();
}


void testTopValue() {
    stack myStack;
    testStart("topValue");
    initStack(&myStack, N);    
    /* stack が空なら NaN を返す */
    assert_equals_int(isnan(TopValue(&myStack)), 1); 
    /* stack が空でなければスタックのさしている数字を返す */
    myStack.top = 1;
    myStack.data[1] = 5.0;
    assert_equals_int(isnan(TopValue(&myStack)), 0); // not NaN
    assert_equals_double(TopValue(&myStack), 5.0);
    myStack.top = 2;
    myStack.data[2] = 4.0;
    assert_equals_int(isnan(TopValue(&myStack)), 0); // not NaN
    assert_equals_double(TopValue(&myStack), 4.0);
    testEnd();
}
void testPush() {
    stack myStack;
    testStart("Push");
    initStack(&myStack, N); //3.1
    myStack.top = N;
    push(&myStack, 5.0); //3.2
    assert_equals_double(TopValue(&myStack), 5.0); //3.3
    assert_equals_int(myStack.top, N-1); //3.4
    push(&myStack, 3.0);
    assert_equals_double(TopValue(&myStack), 3.0); //3.4
    assert_equals_int(myStack.top, N-2); //3.5
    testEnd();
}

void testPull() {
    stack myStack;
    testStart("Pull");
    initStack(&myStack, N); //3.1
    myStack.top = N;
    push(&myStack, 5.0); //3.2
    push(&myStack, 3.0);
    assert_equals_double(pull(&myStack), 3.0); //3.3
    assert_equals_int(myStack.top, N-1); //3.4
    assert_equals_double(pull(&myStack), 5.0); //3.3
    assert_equals_int(myStack.top, N);
    assert_equals_int(isnan(TopValue(&myStack)), 1);
    testEnd();
}

void testPrintStack() {
    stack myStack;
    initStack(&myStack, N); //3.1
    myStack.top = N;
    push(&myStack, 5.0); //3.2
    push(&myStack, 3.0);
    push(&myStack, 1.0);
    push(&myStack, 1.0);
    push(&myStack, 533.0);
	printStack(&myStack);
}

void testAddStack(){
	stack myStack;
	testStart("AddStack");
	initStack(&myStack, N);
	myStack.top = N;
	push(&myStack, 5.0);
	push(&myStack, 3.0);
	printf("\n*Current Stack Numbers*\n");
	printStack(&myStack);
	addStack(&myStack);
	printStack(&myStack);
	assert_equals_double(TopValue(&myStack),8.0);
	assert_equals_int(myStack.top, N-1);
	addStack(&myStack);
	assert_equals_int(isnan(TopValue(&myStack)),1);
	assert_equals_int(myStack.top, N-1);
	testEnd();
}

void testSubStack(){
	stack myStack;
	testStart("SubStack");
	initStack(&myStack, N);
	myStack.top = N;
	push(&myStack,3.0);
	push(&myStack,2.0);
	printStack(&myStack);
	subStack(&myStack);
	printStack(&myStack);
	assert_equals_double(TopValue(&myStack),1.0);
	assert_equals_int(myStack.top, N -1 );
	subStack(&myStack);
	assert_equals_int(isnan(TopValue(&myStack)), 1);
	assert_equals_int(myStack.top, N-1);
	testEnd();
}

void testMulStack(){
	stack myStack;
	testStart("MulStack");
	initStack(&myStack, N);
	myStack.top = N;
	push(&myStack,3.0);
	push(&myStack,2.0);
	printStack(&myStack);
	mulStack(&myStack);
	printStack(&myStack);
	assert_equals_double(TopValue(&myStack),6.0);
	assert_equals_int(myStack.top, N -1 );
	mulStack(&myStack);
	assert_equals_int(isnan(TopValue(&myStack)), 1);
	assert_equals_int(myStack.top, N-1);
	testEnd();
}

void testDivStack(){
	stack myStack;
	testStart("DivStack");
	initStack(&myStack, N);
	myStack.top = N;
	push(&myStack,3.0);
	push(&myStack,2.0);
	printStack(&myStack);
	divStack(&myStack);
	printStack(&myStack);
	assert_equals_double(TopValue(&myStack),1.5);
	assert_equals_int(myStack.top, N -1 );
	divStack(&myStack);
	assert_equals_int(isnan(TopValue(&myStack)), 1);
	assert_equals_int(myStack.top, N-1);
	testEnd();
}

int main() {
    testinitStack();
    testTopValue();
    testPush();
    testPull();
    testPrintStack();
	testAddStack();
	testSubStack();
	testMulStack();
	testDivStack();
	return 0;
}
