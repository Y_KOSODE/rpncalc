// stack.h by *****
#define N 10000
typedef struct {
    double data[N];
    int top;
} stack;
void initStack(stack *sp, int n);
double TopValue(stack *sp);
void push(stack *sp, double x);
double pull(stack *sp);
void printStack(stack *sp); // スタックの中身を表示(最後に入力した物が一番最後に表示されること)
void addStack(stack *sp); // 加算
void subStack(stack *sp); // 減算
void mulStack(stack *sp); // 乗算
void divStack(stack *sp); // 除算

