#include <stdio.h>
#include <stdlib.h>
#include "stack.h"
#include <string.h>

#define L 200

int main() {
	stack calc;
	char line[L];
	initStack(&calc, N);
	double data;

	while(1){
	fgets(line, L, stdin);
	printf("\n");	
	if(strcmp(line, "+\n") == 0){
		addStack(&calc);
	
	} else if(strcmp(line, "-\n") == 0){
		subStack(&calc);
		
	} else if(strcmp(line, "*\n") == 0){
		mulStack(&calc);
		
	} else if(strcmp(line, "/\n") == 0){
		divStack(&calc);
		
	} else if(strcmp(line, "e\n") == 0){
		break;
		
	} else if(strcmp(line, "p\n") == 0){
		pull(&calc);
	} else {
		data = atof(line);
		push(&calc, data);
	}
	printStack(&calc);
	}
	return 0;
}
